# cloudera on vm



## Getting started

### Importing the Cloudera QuickStart Image

You can import the Cloudera QuickStart image from Docker Hub:

```bash
docker pull cloudera/quickstart:latest
```


```bash
docker run --hostname=quickstart.cloudera --privileged=true -t -i [OPTIONS] [IMAGE] /usr/bin/docker-quickstart

docker run -m 4G --memory-reservation 2G --memory-swap 8G --hostname=quickstart.cloudera --privileged=true -it --publish-all=true -p 8888:8888 cloudera/quickstart:latest /usr/bin/docker-quickstart

docker run -m 4G --memory-reservation 2G --memory-swap 8G --hostname=quickstart.cloudera --privileged=true -t -i -v $(pwd):/zaid --publish-all=true -p 8888:8888 cloudera/quickstart /usr/bin/docker-quickstart

docker run --hostname=quickstart.cloudera --privileged=true -it --publish-all=true \
 -v $(pwd):/zaid 
 -p "8888:8888" \
 -p "10000:10000" \
 -p "10020:10020" \
 -p "11000:11000" \
 -p "18080:18080" \
 -p "18081:18081" \
 -p "18088:18088" \
 -p "19888:19888" \
 -p "21000:21000" \
 -p "21050:21050" \
 -p "2181:2181" \
 -p "25000:25000" \
 -p "25010:25010" \
 -p "25020:25020" \
 -p "50010:50010" \
 -p "50030:50030" \
 -p "50060:50060" \
 -p "50070:50070" \
 -p "50075:50075" \
 -p "50090:50090" \
 -p "60000:60000" \
 -p "60010:60010" \
 -p "60020:60020" \
 -p "60030:60030" \
 -p "7180:7180" \
 -p "7183:7183" \
 -p "7187:7187" \
 -p "80:80" \
 -p "8020:8020" \
 -p "8032:8032" \
 -p "8042:8042" \
 -p "8088:8088" \
 -p "8983:8983" \
 -p "9083:9083" \
 cloudera/quickstart:latest /usr/bin/docker-quickstart

```

ref : [How to install and running Cloudera Docker Container on Ubuntu](https://dataakkadian.medium.com/how-to-install-and-running-cloudera-docker-container-on-ubuntu-b7c77f147e03)


### Create Cloudera image

```bash
docker build -f Dockerfile -t tokdev/cloudera-quickstart:latest -t tokdev/cloudera-quickstart:0.0.1 .

```