# 
echo "Start install";
echo "#################";
echo "Use one of the following commands to install the Cloudera Manager Agent packages:";

# sudo apt-get install cloudera-manager-agent cloudera-manager-daemons

echo "Install docker";

sudo apt update
sudo apt install -y docker.io
sudo apt install -y docker-compose

sudo usermod -aG docker $USER


echo "Login docker";
docker login --username=${USER}

echo "Install Quickstart cloudera";

docker pull cloudera/quickstart:latest
docker run --name=quickstart.cloudera --hostname=quickstart.cloudera --privileged=true -itd --publish-all=true -p8888 -p8088 cloudera/quickstart:latest /usr/bin/docker-quickstart

