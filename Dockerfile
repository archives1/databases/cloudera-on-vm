FROM cloudera/quickstart:latest

# # Install packages 
# RUN yum update
# RUN update-alternatives --config 'java'
# RUN yum install -y java-1.8.0-openjdk

# # Set environment variables 
# ENV JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk 
# ENV PATH=$PATH:$JAVA_HOME/bin 

# # Download and install Cloudera Manager 
# RUN wget http://archive.cloudera.com/cm5/installer/latest/cloudera-manager-installer.bin && \ 
# chmod u+x cloudera-manager-installer.bin && \ 
# ./cloudera-manager-installer.bin --noprompt --skip_repo_package=true --skip_db_check=true --skip_db_setup=true --skip_db_populate=true --skip_db_validate=true --skip_db_upgrade=true && \ 
# rm -f cloudera-manager-installer.bin 

# # Initial Cloudera
# RUN /usr/bin/docker-quickstart

CMD /home/cloudera/cloudera-manager --express

## Start Cloudera Manager Server and Agent services  
# CMD service cloudera-scm-server start && service cloudera-scm-agent start